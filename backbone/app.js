$(function(){

  var Routes = Backbone.Router.extend({
    routes: {
      "todos/:todo_id": "todo"
    },

    initialize: function() {
    },

    todo: function(todo_id) {
      console.log(Todos.get(todo_id).get('title'))
    }
  });

  var Todo = Backbone.Model.extend({
    defaults: function() {
      return {
        title: "empty todo...",
        items: []
      };
    },
  });

  var TodoCollection = Backbone.Collection.extend({
    model: Todo,
    url: 'http://private-55f0e-altodo.apiary-mock.com/todos',
    parse: function(data) {
      return data.todos;
    }
  });

  //VIEWS

  var TodoListItem = React.createClass({
    render: function() {
      return <li key={this.props.todo.url}>{this.props.todo.title}</li>;
    }
  });

  var TodoList = React.createClass({
    render: function() {
      var todos = this.props.todo_items.map(function (todo) {
          return <TodoListItem todo={todo} />;
      });

      return <ul>{todos}</ul>;
    }
  });

  var TodoListLists = React.createClass({
    render: function() {
      var todolists = this.props.data.models.map(function (todo) {
          return (
            <div>
              <h1>{todo.get('title')}</h1>
              <TodoList todo_items={todo.get('items')} />
            </div>
          );
      });

      return <div>{todolists}</div>;
    }
  });

  var TodosView = Backbone.View.extend({
    el: '#todos',

    initialize: function() {
      this.listenTo(this.collection, 'sync change', this.render);
      this.collection.fetch();
      this.render();
    },

    component: function () {
      return new MainComponent({
        router: Route
      });
    },

    render: function() {
      React.render(<TodoListLists data={this.collection} />, this.el);
      return this;
    },
  });


  //INIT APP
  var Route = new Routes;
  var Todos = new TodoCollection;
  var todosList = new TodosView({collection: Todos});

  Backbone.history.start({pushState: true})

  $(document).on("click", "a[href]:not([data-bypass])", function(evt) {
    var href = { prop: $(this).prop("href"), attr: $(this).attr("href") };
    var root = location.protocol + "//" + location.host;
    if (href.prop.slice(0, root.length) === root) {
      evt.preventDefault();
      Backbone.history.navigate(href.attr, true);
    }
  });
});



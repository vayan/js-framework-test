import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('todos');
  this.route('todo', { path: '/todos/:todo_id' });
});

export default Router;

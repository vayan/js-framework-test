var todoApp = angular.module('todoApp', [ 'ngRoute', 'ngResource' ] );

todoApp.config(function($routeProvider, $locationProvider) {
  $routeProvider
    .when('/todos',  { templateUrl: 'list.html', controller: 'todoListController' })
    .when('/todos/:todo_id',  { templateUrl: 'show.html', controller: 'todoShowController' })
    .otherwise({ redirectTo: '/todos' });
});

todoApp.controller('todoListController', function($scope, $location, $http, Todo) {
  Todo.query(function(result) {
    $scope.todos = result;
  });

  $scope.add = function(todo) {
    Todo.save($scope.newTodoModel, $location.path("todos"));
  }
});

todoApp.controller('todoShowController', function($scope, $location, $http, $routeParams, Todo) {
  Todo.get({todo_id: $routeParams.todo_id}, function(result) {
    $scope.todo = result;
  });

  $scope.update = function(todo) {
    todo.$update($location.path("todos"));
  }
});

todoApp.factory("Todo", ['$resource',function($resource) {
  return $resource('http://private-55f0e-altodo.apiary-mock.com/todos/:todo_id',
    {todo_id:'@todo_id'},
    { update: { method: 'PUT' }}
  );
}]);
